﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GitHubFS
{
    public partial class DebugWindow : Form
    {

        private static DebugWindow staticFormD;
        Image bgImage;


        public DebugWindow()
        {
            //staticFormD.dataGridView1.Ba
            //this.dataGridView1.Opac 

            //bgImage = Image.FromFile(@"C:\Users\Администратор\Desktop\github_small_2.png");
            //bgImage = Image.FromFile(@"C:\Users\Администратор\Downloads\1403337424_github.png");
            
            staticFormD = this;
            InitializeComponent();
        }

        internal static void DoSomeStuffWithTextBox(string fname)
        {
            StackTrace stackTrace = new StackTrace();           // get call stack
            StackFrame[] stackFrames = stackTrace.GetFrames();  // get method calls (frames)

         

            Form1.Dispatcher.Invoke(staticFormD, () =>
            {

                
                staticFormD.dataTable1.Rows.Add(stackFrames[1].GetMethod().Name, fname, DateTime.Now );
                staticFormD.dataGridView1.CurrentCell =
                    staticFormD.dataGridView1.Rows[staticFormD.dataGridView1.Rows.Count - 1].Cells[0];
                
              
                //staticFormD.Text = staticFormD.Text + fname;
                //staticFormD.Show();
            });
        }

        private void DebugWindow_Load(object sender, EventArgs e)
        {
            //dataGridView1.FirstDisplayedScrollingRowIndex = dataGridView1.RowCount - 1;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void FormDebug_Close(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Visible = false;
        }

        internal static void FormDebug_Show()
        {
            staticFormD.Visible = true;
        }

        private void dataGridView1_Paint(object sender, PaintEventArgs e)
        
        {
            int width = this.Size.Width - 220;
            int height = this.Size.Height - 128;

           
            //e.Graphics.DrawImageUnscaled(bgImage, new Point(width, height));
            
            
        }

    }
}
