﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;
using System.Web.Script.Serialization;
using System.Collections;
using Dokan;



namespace GitHubFS
{

    class GitHubRest

    {
         static public object Get(string url) 
         {

             string fullapiurl = @"https://api.github.com" + url;
             HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(fullapiurl);
             req.UserAgent = "GitHubFS Client 0.1";

             HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

             StreamReader stream = new StreamReader(resp.GetResponseStream(), Encoding.UTF8);

             string jsonResponse = stream.ReadToEnd();


             var serialJs = new JavaScriptSerializer();
             serialJs.MaxJsonLength = 10000024;

             object o = serialJs.Deserialize<Dictionary<string, object>>(jsonResponse);             
             return o;

             //Dictionary<string, object> treesMaster = serialJs.Deserialize<Dictionary<string, object>>(jsonResponse);
             //return treesMaster;
             //ArrayList tree = (ArrayList)treesMaster["content"];

             //string content = treesMaster["content"].ToString();
             //Console.WriteLine(content);

         }

    }


    class GitHubFS
    {
        public bool Success = false;
        public string Owner;
        public string ReposName;
        //public string MountPoint;
        public int RepoSize = 0;
        public int CountFiles = 0;
        public int CountFolders = 0;



        public Dictionary<string, Dictionary<string, object>> Tree = new Dictionary<string, Dictionary<string, object>>();

        public GitHubFS(string url)
        {

            Regex regex = new Regex(@"https://github.com/(?<owner>[^/]*)/(?<name>[^/]*)");
            Match match = regex.Match(url);
            if (match.Success)
            {
                this.Owner = match.Groups["owner"].Value;
                this.ReposName = match.Groups["name"].Value;
                this.Success = true;
            }
           
        }

        public object Trees()
        {
            object response = GitHubRest.Get(String.Format(@"/repos/{0}/{1}/git/trees/master?recursive=1", this.Owner, this.ReposName));

            Dictionary<string, object> tree = (Dictionary<string, object>)response;
            ArrayList treeList = (ArrayList)tree["tree"];

            string path,type;
            
            foreach (Dictionary<string,object> info in treeList)
            {
       
                
                path = @"\" + new Regex(@"/").Replace(info["path"].ToString(), @"\");         

                type = info["type"].ToString();
                int position = path.LastIndexOf(@"\");
                string filename = path.Substring(position + 1, path.Length - position - 1);
                string path_to_file = path.Substring(0, position);

                if (path_to_file == String.Empty)
                {
                    path_to_file = @"\";
                }
               
                // File
                if (type == "blob")
                {
        
                    FileInformation fileinfo = new FileInformation();
                    fileinfo.Attributes = FileAttributes.Normal;
                    fileinfo.FileName = filename;                  
                    fileinfo.LastAccessTime = DateTime.Now;
                    fileinfo.LastWriteTime = DateTime.Now;
                    fileinfo.CreationTime = DateTime.Now;
                    fileinfo.Length = (long)(int)info["size"];
               

                    if (!this.Tree.ContainsKey(path_to_file))
                    {
                        this.Tree.Add(path_to_file, new Dictionary<string, object>());                        
                    }

                    this.Tree[path_to_file].Add(filename, fileinfo);
                    
                    //string path_file = 
                    this.RepoSize += (int)info["size"];
                    this.CountFiles += 1;                    
                    //this.Tree.Add(path, new Dictionary<string, object>());
                }
                else
                {



                    FileInformation fileinfo = new FileInformation();
                    fileinfo.Attributes = FileAttributes.Directory;
                    fileinfo.FileName = filename;
                    fileinfo.LastAccessTime = DateTime.Now;
                    fileinfo.LastWriteTime = DateTime.Now;
                    fileinfo.CreationTime = DateTime.Now;
                    fileinfo.Length = (long)(int)0;


                    if (!this.Tree.ContainsKey(path))
                    {
                        this.Tree.Add(path, new Dictionary<string, object>());
                    }

                    if (!this.Tree.ContainsKey(path_to_file))
                    {
                        this.Tree.Add(path_to_file, new Dictionary<string, object>());
                    }

                    this.Tree[path_to_file].Add(filename, fileinfo);

                    this.CountFolders += 1;
                    
                    //this.Tree.Add(path,);
                }
            }            
           
            return tree;
        }

        public void Mount(string uri)
        {

            
  
            DokanOptions opt = new DokanOptions();


            opt.MountPoint = uri;
            opt.DebugMode = false;
            opt.RemovableDrive = true;
            opt.UseStdErr = false;
            opt.VolumeLabel = "GitHubFS";


            int status = DokanNet.DokanMain(opt, new GitHubFSDokan(this));
            switch (status)
            {
                case DokanNet.DOKAN_DRIVE_LETTER_ERROR:

                    //MessageBox.Show("Drvie letter error");
                    break;
                case DokanNet.DOKAN_DRIVER_INSTALL_ERROR:
                    //MessageBox.Show("Driver install error");
                    break;
                case DokanNet.DOKAN_MOUNT_ERROR:
                    //MessageBox.Show("Mount error");
                    break;
                case DokanNet.DOKAN_START_ERROR:
                    //MessageBox.Show("Start error");
                    break;
                case DokanNet.DOKAN_ERROR:
                    //MessageBox.Show("Unknown error");
                    break;
                case DokanNet.DOKAN_SUCCESS:
                    //MessageBox.Show("Success");
                    break;
                default:
                    //MessageBox.Show(status.ToString());
                    break;

            }
        }


    }
}
