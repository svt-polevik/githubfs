﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dokan;
using System.Collections;
using System.IO;
using Microsoft.Win32;
using System.Threading;
using System.Net;
using System.Text.RegularExpressions;
using System.Web.Caching;
using System.Web;

namespace GitHubFS
{


    class GitHubFSDokan : DokanOperations
    {

        private string root_;
        private int count_;
        public Dictionary<string, Dictionary<string, object>> Tree = 
            new Dictionary<string, Dictionary<string, object>>();
        string Owner, ReposName;


        public GitHubFSDokan(GitHubFS  t )
        {

            this.Tree = t.Tree;
            this.Owner = t.Owner;
            this.ReposName = t.ReposName;

            root_ = @"D:\msysgit-master";
            count_ = 1;
        }

        private string GetPath(string filename)
        {
            string path = root_ + filename;
            Console.Error.WriteLine("GetPath : {0}", path);
            return path;
        }

        public int CreateFile(String filename, FileAccess access, FileShare share,
            FileMode mode, FileOptions options, DokanFileInfo info)
        {

            DebugWindow.DoSomeStuffWithTextBox(filename);


            int position = filename.LastIndexOf(@"\");
            string name = filename.Substring(position + 1, filename.Length - position - 1);
            string path_to_file = filename.Substring(0, position);
            if (path_to_file == String.Empty)
            {
                path_to_file = @"\";
            }


            if (this.Tree.ContainsKey(filename))
            {
                info.IsDirectory = true;
                return 0;
            }
            else if (this.Tree.ContainsKey(path_to_file) && this.Tree[path_to_file].ContainsKey(name))
            {
                return 0;
            }
            else
            {
                return -DokanNet.ERROR_FILE_NOT_FOUND;
            }

            
        }

        public int OpenDirectory(String filename, DokanFileInfo info)
        {
            info.Context = count_++;
            if (this.Tree.ContainsKey(filename))
                return 0;
            else
                return -DokanNet.ERROR_PATH_NOT_FOUND;
        }

        public int CreateDirectory(String filename, DokanFileInfo info)
        {
            return -1;
        }

        public int Cleanup(String filename, DokanFileInfo info)
        {
            //Console.WriteLine("%%%%%% count = {0}", info.Context);
            return 0;
        }

        public int CloseFile(String filename, DokanFileInfo info)
        {
            return 0;
        }


        public void RemovedCallback(String k, Object v, CacheItemRemovedReason r)
        {
           
        }


        public int ReadFile(String filename, Byte[] buffer, ref uint readBytes,
            long offset, DokanFileInfo info)
        {
            
            CacheItemRemovedCallback onRemove = new CacheItemRemovedCallback(this.RemovedCallback);

            string fullnamekey = filename;


            HttpContext context = HttpContext.Current;
            if (context == null)
            {
                HttpRequest request = new HttpRequest(string.Empty, "http://tempuri.org", string.Empty);
                HttpResponse response = new HttpResponse(new StreamWriter(new MemoryStream()));
                context = new HttpContext(request, response);
                HttpContext.Current = context;
            }
            
            byte[] data;

            if (context.Cache[fullnamekey] == null)
            {
                DebugWindow.DoSomeStuffWithTextBox(" Get from remote http " + fullnamekey);
                try
                {
                    filename = new Regex(@"\\").Replace(filename, @"/");


                    // Fetch the file contents.
                    WebClient webClient = new WebClient();
                    string fileUri = @"https://raw.githubusercontent.com/" + this.Owner + "/" + this.ReposName + "/master" + filename;
                    data = webClient.DownloadData(fileUri);
                    DebugWindow.DoSomeStuffWithTextBox(" Add to cache " + fullnamekey);
                    context.Cache.Add(fullnamekey, data, null, DateTime.Now.AddSeconds(60), Cache.NoSlidingExpiration, CacheItemPriority.High, onRemove);

                }
                catch (Exception)
                {
                    return -1;
                }
            }
            else
            {
                DebugWindow.DoSomeStuffWithTextBox(" Get from Cache " + fullnamekey);
                data = (byte[])context.Cache[fullnamekey];
            }


            Stream stream = new MemoryStream(data);

            stream.Seek(offset, SeekOrigin.Begin);
            readBytes = (uint)stream.Read(buffer, 0, buffer.Length);

            return 0;
            

        }

        public int WriteFile(String filename, Byte[] buffer,
            ref uint writtenBytes, long offset, DokanFileInfo info)
        {
            return -1;
        }

        public int FlushFileBuffers(String filename, DokanFileInfo info)
        {
            return -1;
        }

        public int GetFileInformation(String filename, FileInformation fileinfo, DokanFileInfo info)
        {


            int position = filename.LastIndexOf(@"\");
            string name = filename.Substring(position + 1, filename.Length - position - 1);
            string path_to_file = filename.Substring(0, position);
            if (path_to_file == String.Empty)
            {
                path_to_file = @"\";
            }

            if (this.Tree.ContainsKey(filename))
            {
                fileinfo.Attributes = System.IO.FileAttributes.Directory;
                fileinfo.LastAccessTime = DateTime.Now;
                fileinfo.LastWriteTime = DateTime.Now;
                fileinfo.CreationTime = DateTime.Now;

                return 0;
            }

            else if (this.Tree.ContainsKey(path_to_file) && this.Tree[path_to_file].ContainsKey(name))
            {
        
               FileInformation f = (FileInformation)this.Tree[path_to_file][name];

               fileinfo.Attributes = System.IO.FileAttributes.Normal;
               fileinfo.LastAccessTime = DateTime.Now;
               fileinfo.LastWriteTime = DateTime.Now;
               fileinfo.CreationTime = DateTime.Now;
               fileinfo.Length = f.Length;
               return 0;
  
            }
            else
            {
                return -1;
            }

        }

        public int FindFiles(String filename, ArrayList files, DokanFileInfo info)
        {
            if (this.Tree.ContainsKey(filename))
            {
                foreach (KeyValuePair<string, object> item in this.Tree[filename])
                {
                    FileInformation finfo = (FileInformation)item.Value;
                    files.Add(finfo);
                }
                return 0;
            }
            else
            {
                return -1;
            }
           
        }

        public int SetFileAttributes(String filename, FileAttributes attr, DokanFileInfo info)
        {
            return -1;
        }

        public int SetFileTime(String filename, DateTime ctime,
                DateTime atime, DateTime mtime, DokanFileInfo info)
        {
            return -1;
        }

        public int DeleteFile(String filename, DokanFileInfo info)
        {
            return -1;
        }

        public int DeleteDirectory(String filename, DokanFileInfo info)
        {
            return -1;
        }

        public int MoveFile(String filename, String newname, bool replace, DokanFileInfo info)
        {
            return -1;
        }

        public int SetEndOfFile(String filename, long length, DokanFileInfo info)
        {
            return -1;
        }

        public int SetAllocationSize(String filename, long length, DokanFileInfo info)
        {
            return -1;
        }

        public int LockFile(String filename, long offset, long length, DokanFileInfo info)
        {
            return 0;
        }

        public int UnlockFile(String filename, long offset, long length, DokanFileInfo info)
        {
            return 0;
        }

        public int GetDiskFreeSpace(ref ulong freeBytesAvailable, ref ulong totalBytes,
            ref ulong totalFreeBytes, DokanFileInfo info)
        {
            freeBytesAvailable = 512 * 1024 * 1024;
            totalBytes = 1024 * 1024 * 1024;
            totalFreeBytes = 512 * 1024 * 1024;
            return 0;
        }

        public int Unmount(DokanFileInfo info)
        {
            return 0;
        }

    }


}
